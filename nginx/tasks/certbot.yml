---

- name: Add certbot repo
  tags: [nginx, cerbot]
  become: yes
  apt_repository:
    repo: "ppa:certbot/certbot"
    state: present
  when: "ansible_facts['distribution'] == 'Ubuntu' and ansible_facts['distribution_major_version'] == 18"

- name: Install certbot
  tags: [nginx, cerbot]
  become: yes
  apt:
    name: certbot
    state: "present"
    update_cache: yes
    cache_valid_time: "{{ cache_valid_time_seconds }}"

- name: Ensure well-known location exists
  tags: [nginx, cerbot]
  become: yes
  file:
    name: "{{ nginx_well_known_location }}/.well-known/"
    state: directory
    mode: 0755

- name: Run certbot command to get a cert
  tags: [nginx, cerbot]
  become: yes
  shell: >
    certbot certonly
    --webroot
    --webroot-path "{{ nginx_well_known_location }}"
    --domains "{{ nginx_dns_name }}"
    --non-interactive
    --agree-tos
    --email "{{ nginx_certbot_email }}"
  register: result
  changed_when: '"Certificate not yet due for renewal; no action taken." not in result.stdout'

- name: Fetch additional configs
  tags: [nginx, cerbot]
  become: yes
  get_url:
    url: "{{ item.url }}"
    dest: "{{ item.dest }}"
    force: yes
    owner: root
    group: root
    mode: 0644
    backup: yes
  with_items:
    - url:  "https://raw.githubusercontent.com/certbot/certbot/master/certbot-nginx/certbot_nginx/_internal/tls_configs/options-ssl-nginx.conf"
      dest: "/etc/letsencrypt/options-ssl-nginx.conf"
    - url: "https://raw.githubusercontent.com/certbot/certbot/master/certbot/certbot/ssl-dhparams.pem"
      dest: "/etc/letsencrypt/ssl-dhparams.pem"
  notify:
    - Reload nginx config


- name: Update service file
  tags: [nginx, cerbot]
  become: yes
  copy:
    src: files/certbot_service
    dest: /lib/systemd/system/certbot.service
    owner: root
    group: root
    mode: 0644
    backup: yes
