#!/usr/bin/env bash

ANSIBLE_CONFIG_PATH=${ANSIBLE_CONFIG_PATH}
INVENTORY_PATH=${INVENTORY_PATH}

cat <<EOF > ${ANSIBLE_CONFIG_PATH}
[defaults]
roles_path = ${COMMON_ROLES_DIR}:~/.ansible/roles:/usr/share/ansible/roles:/etc/ansible/roles
inventory = ${INVENTORY_PATH}
EOF
