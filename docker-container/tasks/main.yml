---

- name: Install dependencies
  tags: [docker-container]
  become: yes
  apt:
    name:
      - docker.io
      - python3-pip
      - python3-setuptools
    state: present
    update_cache: yes
    cache_valid_time: "{{ cache_valid_time_seconds }}"

- name: Ensure docker is enabled as a service
  tags: [docker-container]
  become: yes
  systemd:
    service: docker.service
    enabled: yes
    state: started

- name: Install docker for python
  tags: [docker-container]
  become: yes
  pip:
    name: docker == 4.0.2
    state: present
    executable: /usr/bin/pip3

- name: Login to our registry
  tags: [docker-container]
  become: yes
  docker_login:
    registry_url: "{{ docker_container_registry_url }}"
    username: "{{ docker_container_registry_username }}"
    password: "{{ docker_container_registry_password }}"
  when: "docker_container_registry_url | length > 0"

- name: Load local image
  tags: [docker-container]
  become: yes
  # docs: https://docs.ansible.com/ansible/latest/modules/docker_image_module.html
  docker_image:
    source: load
    state: present
    name: "{{ docker_container_image_name }}"
    load_path: "{{ docker_container_image_file_directory }}/{{ docker_container_image_file_name }}"
  when: "docker_container_image_file_name | length > 0"

- name: Pull image
  tags: [docker-container]
  become: yes
  docker_image:
    source: pull
    state: present
    force_source: "{{ docker_container_image_force_source }}"
    name: "{{ docker_container_image_name }}"
  when: "docker_container_image_file_name | length == 0"

- name: Ensure env files dir exists
  tags: [docker-container]
  become: yes
  file:
    name: "{{ docker_container_docker_env_dir }}"
    state: directory
    mode: 0755

- name: Ensure volume path exists
  tags: [docker-container]
  become: yes
  file:
    name: "{{ item.host_path }}"
    state: "{{ item.state | default('directory') }}"
    mode: "{{ item.mode | default('0755') }}"
    owner: "{{ item.owner | default('root') }}"
    group: "{{ item.group | default('root') }}"
  with_items: "{{ docker_container_volumes }}"

- name: Allow host port through ufw
  tags: [docker-container]
  become: yes
  ufw:
    rule: allow
    port: '{{ item.host_port }}'
    from_ip: "{{ item.from_ip }}"
    proto: tcp
  with_items: "{{ docker_container_ports }}"

- name: Convert port map to list
  tags: [docker-container]
  become: yes
  shell: python3 -c "print(*[':'.join([ i['host_port'], i['container_port'] ]) for i in {{ docker_container_ports }} ], sep='\n')"
  register: list_of_ports

- name: Convert volume map to list
  tags: [docker-container]
  become: yes
  shell: python3 -c "print(*[':'.join([ i['host_path'], i['container_path'] ]) for i in {{ docker_container_volumes }} ], sep='\n')"
  register: list_of_volumes

- name: Drop env file
  tags: [docker-container]
  become: yes
  template:
    src: docker_env.j2
    dest: "{{ docker_container_docker_env_dir }}/{{ docker_container_docker_env_file }}"

- name: Run container
  tags: [docker-container]
  become: yes
  # docs: https://docs.ansible.com/ansible/latest/modules/docker_container_module.html
  docker_container:
    name: "{{ docker_container_container_name }}"
    image: "{{ docker_container_image_name }}"
    env_file: "{{ docker_container_docker_env_dir }}/{{ docker_container_docker_env_file }}"
    ports: "{{ list_of_ports.stdout_lines }}"
    volumes: "{{ list_of_volumes.stdout_lines }}"
    privileged: "{{ docker_container_privileged }}"
    restart_policy: always
    state: started
  when: "docker_container_force_restart == false and not docker_container_command"

- name: Run container with command
  tags: [docker-container]
  become: yes
  # docs: https://docs.ansible.com/ansible/latest/modules/docker_container_module.html
  docker_container:
    name: "{{ docker_container_container_name }}"
    image: "{{ docker_container_image_name }}"
    env_file: "{{ docker_container_docker_env_dir }}/{{ docker_container_docker_env_file }}"
    ports: "{{ list_of_ports.stdout_lines }}"
    volumes: "{{ list_of_volumes.stdout_lines }}"
    command: "{{ docker_container_command }}"
    privileged: "{{ docker_container_privileged }}"
    restart_policy: always
    state: started
  when: "docker_container_force_restart == false and docker_container_command"

- name: Force restart container
  tags: [docker-container]
  become: yes
  # docs: https://docs.ansible.com/ansible/latest/modules/docker_container_module.html
  docker_container:
    name: "{{ docker_container_container_name }}"
    image: "{{ docker_container_image_name }}"
    env_file: "{{ docker_container_docker_env_dir }}/{{ docker_container_docker_env_file }}"
    ports: "{{ list_of_ports.stdout_lines }}"
    volumes: "{{ list_of_volumes.stdout_lines }}"
    privileged: "{{ docker_container_privileged }}"
    restart_policy: always
    state: started
    restart: yes
  when: "docker_container_force_restart == true"

- name: Create network
  tags: [docker-container]
  become: yes
  docker_network:
    name: "{{ docker_container_network }}"
    connected:
      - "{{ docker_container_container_name }}"
    appends: yes
  when: "docker_container_network != false"

- name: Logout
  tags: [docker-container]
  become: yes
  docker_login:
    registry_url: "{{ docker_container_registry_url }}"
    username: "{{ docker_container_registry_username }}"
    state: absent
  when: "docker_container_registry_url | length > 0"
